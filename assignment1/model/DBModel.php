<?php
include_once("IModel.php");
include_once("Book.php");
include_once("constants.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */


class DBModel implements IModel
{        
      /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /** 20
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try{			
			$this->db = new PDO('mysql:host=localhost;dbname=Assignment1;charset=utf8','root','', 
			array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			}
		catch(PDOException $ex){
			ErrorView('Connection failed: ' . $e->getMessage());
        		$view->create();
		}
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id. 
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     40*/
    public function getBookList()
    {
	$booklist = array();
	$boker = $this->db->query('SELECT * FROM book ORDER BY id');
	while($row=$boker->fetch(PDO::FETCH_ASSOC))
	{
		$booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
	}
	return $booklist;	
    }

    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */

    public function getBookById($id)
    {
	$booklist = null;	
	if(filter_var($id, FILTER_VALIDATE_INT) !== FALSE){	
	$boker = $this->db->query("SELECT title, author, description FROM book WHERE id=$id");
	$row=$boker->fetch(PDO::FETCH_ASSOC);
	if($row){
	$booklist = new Book($row['title'], $row['author'], $row['description'],$id);
	}	
	}
	else{
		$view = new errorView('No one with that id');
		$view->create();	
	}
	return $booklist;	
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {	
	try{	
	$opp = $this->db->prepare('INSERT INTO book(title,author,description)'
				 . ' VALUES(:title, :author, :description)'
				);
	$opp->bindValue(':title', $book->title);
	$opp->bindValue(':author', $book->author);
	$opp->bindValue(':description', $book->description);
	if (empty($book->title) && empty($book->author)) {
            $view = new errorView('Write the author and the title ');
            $view->create();
	}
	else if(empty($book->title)){
            $view = new errorView('Write the title ');
            $view->create();
	}
	else if(empty($book->author)){
	    $view = new errorView('Write the author ');
            $view->create();
	}
	else{
		$result=$opp->execute();
		$book->id = $this->db->lastInsertId();
	}	
	} catch (PDOException $e) {
            $view = new errorView('failed to add book: ' . $e->getMessage());
            $view->create();
        }		
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
	try{	
	$oper=$this->db->prepare("UPDATE book SET title=:title, author=:author, description=:description WHERE id=$book->id");
	$oper->bindValue(':title', $book->title);
	$oper->bindValue(':author', $book->author);
	$oper->bindValue(':description', $book->description);
	if (empty($book->title) && empty($book->author)) {
            $view = new errorView('Write the author and the title ');
            $view->create();
	}
	else if(empty($book->title)){
            $view = new errorView('Write the title ');
            $view->create();
	}
	else if(empty($book->author)){
	    $view = new errorView('Write the author ');
            $view->create();
	}
	else{
		$oper->execute();
	}	
	} catch (PDOException $e) {
            $view = new errorView('failed to modify book: ' . $e->getMessage());
            $view->create();
        }	
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
	$slett = $this->db->prepare("DELETE FROM book WHERE id=$id");
	$slett->execute();
    }
	
}

?>
